ruby-color (1.8-4) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Apply multi-arch hints. + ruby-color: Add Multi-Arch: foreign.

  [ Aquila Macedo ]
  * Removed 'debian/source/lintian-overrides' file.
  * d/control: 'debhelper-compat' in Build-Depends is bumped to level 13.
  * d/control: bumped Standards-Version to 4.6.2.
  * d/control: replaced 'ruby-interpreter' to '${ruby:Depends}'.
  * d/watch: updated watch file reference link.
  * d/copyright: updated the upstream URL to the GitHub repository.

 -- Aquila Macedo Costa <aquilamacedo@riseup.net>  Thu, 13 Jul 2023 17:07:57 -0300

ruby-color (1.8-3) unstable; urgency=medium

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Debian Janitor ]
  * Use secure copyright file specification URI.
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update Vcs-* headers from URL redirect.
  * Use canonical URL in Vcs-Git.
  * Apply multi-arch hints.
    + ruby-color: Add :any qualifier for ruby dependency.
  * Update watch file format version to 4.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 01 Jul 2022 00:39:28 +0100

ruby-color (1.8-2) unstable; urgency=medium

  * Drop transitional ruby-color-tool package (Closes: #878857)
  * Bump debhelper compatibility level to 10
  * Bump Standards-Version to 4.1.2 (no changes needed)
  * Update homepage and set testsuite
  * Refresh packaging with dh-make-ruby -w

 -- Cédric Boutillier <boutil@debian.org>  Sun, 03 Dec 2017 00:39:45 +0100

ruby-color (1.8-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Update how tests were executed in the package
  * debian/copyright: Remove nonexistent file from copyright
  * debian/rules: Add check-dependencies command

 -- Lucas Albuquerque Medeiros de Moura <lucas.moura128@gmail.com>  Sat, 05 Mar 2016 18:30:50 -0300

ruby-color (1.7.1-1) unstable; urgency=medium

  * Imported Upstream version 1.7.1

 -- Cédric Boutillier <boutil@debian.org>  Sun, 27 Jul 2014 21:34:25 +0200

ruby-color (1.7-1) unstable; urgency=medium

  * Imported Upstream version 1.7
  * Update email address and years in Upstream authors entry
  * Shorten the description of ruby-color-tools transitional package

 -- Cédric Boutillier <boutil@debian.org>  Sun, 06 Jul 2014 22:58:27 +0200

ruby-color (1.5.1-1) unstable; urgency=medium

  * Imported Upstream version 1.5.1
  * Rename source and binary to ruby-color
    + upstream is now called color instead of color-tools
    + provide a ruby-color-tools dummy transition package
  * Add myself to uploaders
  * Bump standards-version to 3.9.5 (no changes needed)
  * Use the Ruby method to run the tests
  * patch: do not use gem to load minitest

 -- Cédric Boutillier <boutil@debian.org>  Sun, 11 May 2014 00:10:10 +0200

ruby-color-tools (1.4.2-2) unstable; urgency=low

  * Team upload
  * Build-depend on ruby-minitest (Closes: #725548).
  * Modify require_instead_of_gem patch to simply delete the gem statement
    instead of replacing it with a require.
  * Install History.rdoc as upstream changelog.

 -- Cédric Boutillier <boutil@debian.org>  Mon, 07 Oct 2013 15:30:54 +0200

ruby-color-tools (1.4.2-1) unstable; urgency=low

  * New upstream release
  * Dropped transitional packages, no longer needed
  * Adjusted list of test files to what currently ships
  * Replace "gem" calls by "require" in the test cases
  * README.txt renamed to .rdoc

 -- Gunnar Wolf <gwolf@gwolf.org>  Tue, 01 Oct 2013 11:03:35 -0500

ruby-color-tools (1.4.1-2) unstable; urgency=low

  * Team upload.
  * Bump build dependency on gem2deb to >= 0.3.0~.
  * Bump Standard-Version: to 3.9.3 (no changes needed)
  * Use Breaks instead of Conflicts for dependency relation on old packages
  * Set priority of transitional packages to extra
  * Override lintian warning about duplicate description of transitional
    packages

 -- Cédric Boutillier <cedric.boutillier@gmail.com>  Sat, 30 Jun 2012 15:51:47 +0200

ruby-color-tools (1.4.1-1) unstable; urgency=low

  * New upstream release
  * Switched to gem2deb build system; renamed package libcolor-tools-
    ruby → ruby-color-tools

 -- Gunnar Wolf <gwolf@debian.org>  Wed, 08 Jun 2011 13:08:46 -0500

libcolor-tools-ruby (1.4.0-1) unstable; urgency=low

  * Changed section to Ruby as per ftp-masters' request
  * Updated debian/watch as the project was separated from pdf-writer
  * Standards-version 3.7.2 => 3.8.1.0 (no changes needed)
  * Added Vcs-Svn, Vcs-Browser and Homepage tags to debian/control
  * New upstream release

 -- Gunnar Wolf <gwolf@debian.org>  Sat, 25 Apr 2009 14:10:27 -0500

libcolor-tools-ruby (1.3.0-1) unstable; urgency=low

  * Initial upload (Closes: #442082)

 -- Gunnar Wolf <gwolf@debian.org>  Wed, 12 Sep 2007 20:39:03 -0500
